#!/bin/sh
# This script is run by debian installer using preseed/late_command
# directive, see preseed.cfg

# Setup console, remove timeout on boot.
CONSOLE=ttyS0
KERNEL_PARAMS="biosdevname=0 console=tty1 console=$CONSOLE,115200n8 earlyprintk=$CONSOLE,115200n8 consoleblank=0 systemd.show_status=true"
ROOTVOL=`ls /dev/mapper/ -1 | grep root`
sed -i 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=3/g; s/GRUB_TERMINAL=.*/GRUB_TERMINAL=\"console serial\"/g; s/^[ #\t]*GRUB_DISABLE_LINUX_UUID=true/#GRUB_DISABLE_LINUX_UUID=true/g' /etc/default/grub
sed -i -e 's|^[ #\t]*GRUB_CMDLINE_LINUX_DEFAULT[ #\t]*=.*|GRUB_CMDLINE_LINUX_DEFAULT="'"${KERNEL_PARAMS}"'"|' /etc/default/grub
sed -i "s/^[ #\t]*GRUB_HIDDEN_TIMEOUT_QUIET=true/GRUB_HIDDEN_TIMEOUT_QUIET=false/" /etc/default/grub
sed -i "s/^[ #\t]*GRUB_HIDDEN_TIMEOUT=/#GRUB_HIDDEN_TIMEOUT=0/" /etc/default/grub
echo "
# openstack-debian-images disables OS prober to avoid
# loopback detection which breaks booting
GRUB_DISABLE_OS_PROBER=true

# Root device name shooud not depend on lvid
#GRUB_DEVICE=/dev/mapper/${ROOTVOL}
#GRUB_DEVICE_BOOT=lvm/${ROOTVOL}" >>/etc/default/grub
update-grub


sed -i "s/^[ #\t]*PermitRootLogin .*/PermitRootLogin yes/" /etc/ssh/sshd_config
cat >>/etc/ssh/sshd_config <<EOF

ClientAliveInterval 120
EOF


echo "# disable pc speaker
blacklist pcspkr" >/etc/modprobe.d/blacklist.conf

if [ -e /etc/kbd/config ] ; then
	sed -i s/^BLANK_TIME=.*/BLANK_TIME=0/ /etc/kbd/config
	sed -i s/^POWERDOWN_TIME=.*/POWERDOWN_TIME=0/ /etc/kbd/config
	sed -i 's/^[ \t#]KEYBOARD_DELAY=.*/KEYBOARD_DELAY=1000/' /etc/kbd/config
fi

#rm -f /etc/ssh/ssh_host_*
rm -f /etc/udev/rules.d/70-persistent-net.rules
rm -f /lib/udev/write_net_rules
rm -f /var/cache/apt/archives/*.deb

SYSTEMD_DIR="/etc/systemd/system/"
for service in emergency.service rescue.service ; do
	mkdir "${SYSTEMD_DIR}/${service}.d"
	echo '[Service]
ExecStart=
ExecStart=-/bin/sh -c "/sbin/sulogin /dev/tty0; /bin/systemctl --fail --no-block default"' > "${SYSTEMD_DIR}/${service}.d/console.conf"
done

# Required to allow LVM resize on boot
sed -i "s/locking_type = 4/locking_type = 1/" /etc/lvm/lvm.conf
sed -i "s/issue_discards = 0/issue_discards = 1/" /etc/lvm/lvm.conf

sed -i -E 's:(.*)\s(/)\s\s*([[:alnum:]]*)\s*([[:alnum:][:punct:]]*)\s*(.*):\1 \2 \3 \4,discard,relatime,nodiratime \5:' /etc/fstab
sed -i -E 's:(.*)\s(\w+)\s\s*(swap)\s*([[:alnum:][:punct:]]*)\s*(.*):\1 \2 \3 \4,discard \5:' /etc/fstab

# Empty message of the day.
echo -n > /etc/motd

echo '# IO depth and optimal scheduler for DDoS-Guard virtualized instances
ACTION=="add|change", SUBSYSTEM=="block", KERNEL=="sd[a-z]", ATTR{queue/max_sectors_kb}="4096", ATTR{queue/read_ahead_kb}="2048", ATTR{queue/scheduler}="deadline"
' >> /etc/udev/rules.d/70-block.rules

cat > /usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py <<EOF
# Copyright (C) 2009-2010 Canonical Ltd.
# Copyright (C) 2012, 2013 Hewlett-Packard Development Company, L.P.
#
# Author: Scott Moser <scott.moser@canonical.com>
# Author: Juerg Haefliger <juerg.haefliger@hp.com>
#
# This file is part of cloud-init. See LICENSE file for license information.

"""
Set Passwords
-------------
**Summary:** Set user passwords

Set system passwords and enable or disable ssh password authentication.
The ``chpasswd`` config key accepts a dictionary containing a single one of two
keys, either ``expire`` or ``list``. If ``expire`` is specified and is set to
``false``, then the ``password`` global config key is used as the password for
all user accounts. If the ``expire`` key is specified and is set to ``true``
then user passwords will be expired, preventing the default system passwords
from being used.

If the ``list`` key is provided, a list of
``username:password`` pairs can be specified. The usernames specified
must already exist on the system, or have been created using the
``cc_users_groups`` module. A password can be randomly generated using
``username:RANDOM`` or ``username:R``. A hashed password can be specified
using ``username:$6$salt$hash``. Password ssh authentication can be
enabled, disabled, or left to system defaults using ``ssh_pwauth``.

.. note::
    if using ``expire: true`` then a ssh authkey should be specified or it may
    not be possible to login to the system

**Internal name:** ``cc_set_passwords``

**Module frequency:** per instance

**Supported distros:** all

**Config keys**::

    ssh_pwauth: <yes/no/unchanged>

    password: password1
    chpasswd:
        expire: <true/false>

    chpasswd:
        list: |
            user1:password1
            user2:RANDOM
            user3:password3
            user4:R

    ##
    # or as yaml list
    ##
    chpasswd:
        list:
            - user1:password1
            - user2:RANDOM
            - user3:password3
            - user4:R
            - user4:$6$rL..$ej...
"""

import re
import sys

from cloudinit.distros import ug_util
from cloudinit import ssh_util
from cloudinit import util

from string import ascii_letters, digits

# We are removing certain 'painful' letters/numbers
PW_SET = (''.join([x for x in ascii_letters + digits
                   if x not in 'loLOI01']))


def handle(_name, cfg, cloud, log, args):
    if len(args) != 0:
        # if run from command line, and give args, wipe the chpasswd['list']
        password = args[0]
        if 'chpasswd' in cfg and 'list' in cfg['chpasswd']:
            del cfg['chpasswd']['list']
    else:
        password = util.get_cfg_option_str(cfg, "password", None)

    # use admin_pass key from metadata
    if not password:
        metadata = cloud.datasource.metadata
        if metadata and 'admin_pass' in metadata['meta']:
            password = metadata['meta']['admin_pass']

    expire = True
    plist = None

    if 'chpasswd' in cfg:
        chfg = cfg['chpasswd']
        if 'list' in chfg and chfg['list']:
            if isinstance(chfg['list'], list):
                log.debug("Handling input for chpasswd as list.")
                plist = util.get_cfg_option_list(chfg, 'list', plist)
            else:
                log.debug("Handling input for chpasswd as multiline string.")
                plist = util.get_cfg_option_str(chfg, 'list', plist)
                if plist:
                    plist = plist.splitlines()

        expire = util.get_cfg_option_bool(chfg, 'expire', expire)

    if not plist and password:
        (users, _groups) = ug_util.normalize_users_groups(cfg, cloud.distro)
        (user, _user_config) = ug_util.extract_default(users)
        if user:
            plist = ["%s:%s" % (user, password)]
        else:
            log.warn("No default or defined user to change password for.")

    errors = []
    if plist:
        plist_in = []
        hashed_plist_in = []
        hashed_users = []
        randlist = []
        users = []
        prog = re.compile(r'\$[1,2a,2y,5,6](\$.+){2}')
        for line in plist:
            u, p = line.split(':', 1)
            if prog.match(p) is not None and ":" not in p:
                hashed_plist_in.append("%s:%s" % (u, p))
                hashed_users.append(u)
            else:
                if p == "R" or p == "RANDOM":
                    p = rand_user_password()
                    randlist.append("%s:%s" % (u, p))
                plist_in.append("%s:%s" % (u, p))
                users.append(u)

        ch_in = '\n'.join(plist_in) + '\n'
        if users:
            try:
                log.debug("Changing password for %s:", users)
                util.subp(['chpasswd'], ch_in)
            except Exception as e:
                errors.append(e)
                util.logexc(
                    log, "Failed to set passwords with chpasswd for %s", users)

        hashed_ch_in = '\n'.join(hashed_plist_in) + '\n'
        if hashed_users:
            try:
                log.debug("Setting hashed password for %s:", hashed_users)
                util.subp(['chpasswd', '-e'], hashed_ch_in)
            except Exception as e:
                errors.append(e)
                util.logexc(
                    log, "Failed to set hashed passwords with chpasswd for %s",
                    hashed_users)

        if len(randlist):
            blurb = ("Set the following 'random' passwords\n",
                     '\n'.join(randlist))
            sys.stderr.write("%s\n%s\n" % blurb)

        if expire:
            expired_users = []
            for u in users:
                try:
                    util.subp(['passwd', '--expire', u])
                    expired_users.append(u)
                except Exception as e:
                    errors.append(e)
                    util.logexc(log, "Failed to set 'expire' for %s", u)
            if expired_users:
                log.debug("Expired passwords for: %s users", expired_users)

    change_pwauth = False
    pw_auth = None
    if 'ssh_pwauth' in cfg:
        if util.is_true(cfg['ssh_pwauth']):
            change_pwauth = True
            pw_auth = 'yes'
        elif util.is_false(cfg['ssh_pwauth']):
            change_pwauth = True
            pw_auth = 'no'
        elif str(cfg['ssh_pwauth']).lower() == 'unchanged':
            log.debug('Leaving auth line unchanged')
            change_pwauth = False
        elif not str(cfg['ssh_pwauth']).strip():
            log.debug('Leaving auth line unchanged')
            change_pwauth = False
        elif not cfg['ssh_pwauth']:
            log.debug('Leaving auth line unchanged')
            change_pwauth = False
        else:
            msg = 'Unrecognized value %s for ssh_pwauth' % cfg['ssh_pwauth']
            util.logexc(log, msg)

    if change_pwauth:
        replaced_auth = False

        # See: man sshd_config
        old_lines = ssh_util.parse_ssh_config(ssh_util.DEF_SSHD_CFG)
        new_lines = []
        i = 0
        for (i, line) in enumerate(old_lines):
            # Keywords are case-insensitive and arguments are case-sensitive
            if line.key == 'passwordauthentication':
                log.debug("Replacing auth line %s with %s", i + 1, pw_auth)
                replaced_auth = True
                line.value = pw_auth
            new_lines.append(line)

        if not replaced_auth:
            log.debug("Adding new auth line %s", i + 1)
            replaced_auth = True
            new_lines.append(ssh_util.SshdConfigLine('',
                                                     'PasswordAuthentication',
                                                     pw_auth))

        lines = [str(l) for l in new_lines]
        util.write_file(ssh_util.DEF_SSHD_CFG, "\n".join(lines),
                        copy_mode=True)

        try:
            cmd = cloud.distro.init_cmd  # Default service
            cmd.append(cloud.distro.get_option('ssh_svcname', 'ssh'))
            cmd.append('restart')
            if 'systemctl' in cmd:  # Switch action ordering
                cmd[1], cmd[2] = cmd[2], cmd[1]
            cmd = filter(None, cmd)  # Remove empty arguments
            util.subp(cmd)
            log.debug("Restarted the ssh daemon")
        except Exception:
            util.logexc(log, "Restarting of the ssh daemon failed")

    if len(errors):
        log.debug("%s errors occured, re-raising the last one", len(errors))
        raise errors[-1]


def rand_user_password(pwlen=9):
    return util.rand_str(pwlen, select_from=PW_SET)

# vi: ts=4 expandtab

EOF
# Unpack postinst tarball.
# tar -x -v -z -C/tmp -f /tmp/postinst.tar.gz
# cat /tmp/postinst/cloud.cfg > /etc/cloud/cloud.cfg
#patch /usr/lib/python3/dist-packages/cloudinit/config/cc_set_passwords.py /tmp/postinst/cc_set_passwords.py.patch
echo "datasource_list: [ ConfigDrive, OpenStack, None ]" > /etc/cloud/cloud.cfg.d/90_dpkg.cfg

# cloud-utils in jessie does not properly support growpart
systemctl enable cloud-init
# Remove some non-essential packages.
DEBIAN_FRONTEND=noninteractive apt-get purge -y laptop-detect tasksel dictionaries-common emacsen-common iamerican ibritish ienglish-common ispell nfs-common rpcbind
DEBIAN_FRONTEND=noninteractive apt-get -y autoremove


echo ' #!/bin/sh

PREREQS=""
case $1 in
    prereqs) echo "${PREREQS}"; exit 0;;
esac

. /usr/share/initramfs-tools/hook-functions

##
copy_exec /sbin/sfdisk /bin
copy_exec /usr/bin/growpart /bin
copy_exec /sbin/udevadm /bin
copy_exec /sbin/e2fsck /bin
copy_exec /sbin/resize2fs /bin
copy_exec /bin/sed /bin
copy_exec /usr/bin/awk /bin
copy_exec /usr/bin/partx /bin
copy_exec /lib/x86_64-linux-gnu/libm.so.6 /lib/x86_64-linux-gnu
copy_exec /lib/x86_64-linux-gnu/libsmartcols.so.1 /lib/x86_64-linux-gnu

' >/etc/initramfs-tools/hooks/growroot

echo ' #!/bin/sh

PREREQS=""
case $1 in
    prereqs) echo "${PREREQS}"; exit 0;;
esac

. /scripts/functions

growroot()
{
        echo "Running ROOTFS-RESIZE ..."
        lvm_pv_path=$(lvm pvs --noheadings |awk '"'"'{print $1}'"'"')
        lvm_pv_temp=$(echo ${lvm_pv_path}|sed "s/dev//g")
        lvm_pv_dev=$(echo ${lvm_pv_temp}| sed "s/[^a-z]//g")
        lvm_pv_part=$(echo ${lvm_pv_temp}| sed "s/[^0-9]//g")
        echo "ROOTFS-RESIZE: rootfs found on disk ${lvm_pv_dev} ${lvm_pv_part}"

				out=$(growpart --dry-run /dev/${lvm_pv_dev} ${lvm_pv_part} 2>&1)
        ret=$?

				case "$ret:$out" in
				  0:CHANGE:*)
					  :
					;;
				  [01]:NOCHANGE:*)
					  echo "ROOTFS-RESIZE: nothing to do"
						return
					;;
				  *)
			      echo "ROOTFS-RESIZE: exited $ret \"${out}\""
			      return
			    ;;
			  esac

        lvm vgchange -an
        growpart -v /dev/${lvm_pv_dev} ${lvm_pv_part}
        lvm pvresize -v ${lvm_pv_path}
        lvm vgchange --sysinit -ay
        lvm lvresize -v -l +100%FREE ${ROOT}
        e2fsck -p -f ${ROOT}
        resize2fs -p ${ROOT}
}


growroot

' >/etc/initramfs-tools/scripts/local-premount/growroot

chmod +x /etc/initramfs-tools/hooks/growroot
chmod +x /etc/initramfs-tools/scripts/local-premount/growroot
update-initramfs -u

#rm -f /etc/ssh/ssh_host*
ln -s /lib/systemd/system/cloud-init.target /etc/systemd/system/multi-user.target.wants/cloud-init.target

# Clear proxy config
echo "" > /etc/apt/apt.conf

echo '
users:
   - default

disable_root: false
ssh_pwauth: true

datasource_list: [ Ec2, OpenStack ]

cloud_init_modules:
 - migrator
 - bootcmd
 - write-files
 - set_hostname
 - update_hostname
 - update_etc_hosts
 - ca-certs
 - rsyslog
 - users-groups
 - ssh

cloud_config_modules:
 - emit_upstart
 - locale
 - set-passwords
 - grub-dpkg
 - apt-pipelining
 - apt-configure
 - package-update-upgrade-install
 - timezone
 - disable-ec2-metadata
 - runcmd


cloud_final_modules:
 - rightscale_userdata
 - scripts-per-once
 - scripts-per-boot
 - scripts-per-instance
 - scripts-user
 - ssh-authkey-fingerprints
 - keys-to-console
 - phone-home
 - final-message
 - power-state-change

syslog_fix_perms: root:root
warnings:
  dsid_missing_source: off
apt_preserve_sources_list: true

system_info:
   distro: debian
   default_user:
     name: root
     lock_passwd: false
   paths:
      cloud_dir: /var/lib/cloud/
      templates_dir: /etc/cloud/templates/
      upstart_dir: /etc/init/' > /etc/cloud/cloud.cfg