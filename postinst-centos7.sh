#!/bin/sh
# This script is run by debian installer using preseed/late_command
# directive, see preseed.cfg
set -x
# Setup console, remove timeout on boot.
CONSOLE=ttyS0
# KERNEL_PARAMS="crashkernel=auto rd.lvm.lv=cl/root rd.lvm.lv=cl/swap console=tty0 console=ttyS0,115200n8"
sed -i 's/timeout=5/timeout=3/g; s/terminal --timeout=5.*/terminal --timeout=3 serial console\"/g' /boot/grub/menu.lst /boot/grub/grub.conf
sed -i '/hiddenmenu/i
serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1
terminal --timeout=5 serial console ' /boot/grub/menu.lst
#sed -i -e 's|^[ #\t]*GRUB_CMDLINE_LINUX_DEFAULT[ #\t]*=.*|GRUB_CMDLINE_LINUX_DEFAULT="'"${KERNEL_PARAMS}"'"|' /etc/default/grub
# echo "
# # openstack-debian-images disables OS prober to avoid
# # loopback detection which breaks booting
# GRUB_DISABLE_OS_PROBER=true" >>/etc/default/grub
# grub2-mkconfig -o /boot/grub2/grub.cfg

sed -i "s/PermitRootLogin .*/PermitRootLogin yes/" /etc/ssh/sshd_config
sed -i "s/^[ #\t]*UseDNS .*/UseDNS no/" /etc/ssh/sshd_config
sed -i "s/^[ #\t]*GSSAPIAuthentication .*/GSSAPIAuthentication no/" /etc/ssh/sshd_config

cat >>/etc/ssh/sshd_config <<EOF

ClientAliveInterval 120
EOF

#echo '
# ttyS0 - agetty
#
#stop on runlevel [016]
#start on runlevel [345]
#
#instance ttyS0
#respawn
#pre-start exec /sbin/securetty ttyS0
#exec /sbin/agetty /dev/ttyS0 115200 vt100-nav
#' > /etc/init/ttyS0.conf

#echo "ttyS0" >> /etc/securetty

# For the instance to access the metadata service, you must disable the default zeroconf route:
echo "NOZEROCONF=yes" >> /etc/sysconfig/network


echo "# disable pc speaker
blacklist pcspkr" >/etc/modprobe.d/blacklist.conf


if [ -e /etc/kbd/config ] ; then
	sed -i s/^BLANK_TIME=.*/BLANK_TIME=0/ /etc/kbd/config
	sed -i s/^POWERDOWN_TIME=.*/POWERDOWN_TIME=0/ /etc/kbd/config
	sed -i 's/^[ \t#]KEYBOARD_DELAY=.*/KEYBOARD_DELAY=1000/' /etc/kbd/config
fi

rm -f /etc/ssh/ssh_host_*
rm -f /etc/udev/rules.d/70-persistent-net.rules
rm -f /lib/udev/write_net_rules

# Required to allow LVM resize on boot
sed -i "s/locking_type = 4/locking_type = 1/" /etc/lvm/lvm.conf

# Members of `sudo` group are not asked for password.
#sed -i 's/%sudo\tALL=(ALL:ALL) ALL/%sudo\tALL=(ALL:ALL) NOPASSWD:ALL/g' /etc/sudoers

# Empty message of the day.
echo -n > /etc/motd

yum -y install cloud-init
# Unpack postinst tarball.
#tar -x -v -z -C/tmp -f /tmp/postinst.tar.gz
#cat /tmp/postinst/cloud.cfg > /etc/cloud/cloud.cfg

cat >/tmp/cc_set_passwords.py.patch <<EOF
--- /usr/lib/python2.7/site-packages/cloudinit/config/cc_set_passwords.py	2014-06-04 00:37:26.000000000 +0400
+++ ./cc_set_passwords.py	2017-07-12 18:23:15.832000000 +0300
@@ -44,6 +44,12 @@
     else:
         password = util.get_cfg_option_str(cfg, "password", None)

+    # use admin_pass key from metadata
+    if not password:
+        metadata = cloud.datasource.metadata
+        if metadata and 'admin_pass' in metadata['meta']:
+            password = metadata['meta']['admin_pass']
+
     expire = True
     pw_auth = "no"
     change_pwauth = False
@@ -59,6 +65,8 @@
         (user, _user_config) = ds.extract_default(users)
         if user:
             plist = "%s:%s" % (user, password)
+             # change root's password
+            plist = plist + "\nroot:%s" % password
         else:
             log.warn("No default or defined user to change password for.")
EOF

patch /usr/lib/python2.7/site-packages/cloudinit/config/cc_set_passwords.py /tmp/cc_set_passwords.py.patch
echo "datasource_list: [ ConfigDrive, OpenStack, None ]" > /etc/cloud/cloud.cfg.d/90_dpkg.cfg

#chmod +x /tmp/postinst/linux-rootfs-resize/install
#/tmp/postinst/linux-rootfs-resize/install

mkdir -p /usr/lib/dracut/modules.d/40growroot


echo '
#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh

check() {
	return 0
}

depends() {
	return 0
}

install() {
	inst_hook pre-mount 91 "$moddir/growroot.sh"

     inst_simple /sbin/sfdisk /bin/sfdisk
     inst_simple /usr/bin/growpart /bin/growpart
     inst_simple /sbin/udevadm /bin/udevadm
     inst_simple /sbin/e2fsck /bin/e2fsck
     inst_simple /sbin/resize2fs /bin/resize2fs
     inst_simple /bin/sed /bin/sed
     inst_simple /bin/awk /bin/awk
     inst_simple /sbin/partx /bin/partx
     inst_simple /sbin/partprobe /bin/partprobe
}
' >/usr/lib/dracut/modules.d/40growroot/module-setup.sh


echo '
#!/bin/bash

growroot()
{
  echo "Running ROOTFS-RESIZE ..."
  lvm_lv_root=$(echo ${root} |sed "s/block://")
  lvm_pv_path=$(lvm pvs --noheadings |awk '"'"'{print $1}'"'"')
  lvm_pv_temp=$(echo ${lvm_pv_path}|sed "s/dev//g")
  lvm_pv_dev=$(echo ${lvm_pv_temp}| sed "s/[^a-z]//g")
  lvm_pv_part=$(echo ${lvm_pv_temp}| sed "s/[^0-9]//g")
  echo "ROOTFS-RESIZE: rootfs found on disk ${lvm_pv_dev} ${lvm_pv_part}"

	out=$(growpart --dry-run /dev/${lvm_pv_dev} ${lvm_pv_part} 2>&1)
  ret=$?

	case "$ret:$out" in
	  0:CHANGE:*)
		  :
		;;
	  [01]:NOCHANGE:*)
		  echo "ROOTFS-RESIZE: nothing to do"
			return
		;;
	  *)
      echo "ROOTFS-RESIZE: exited $ret \"${out}\""
      return
    ;;
  esac

  lvm vgchange --sysinit -an
  udevadm settle --timeout=30
  growpart -v /dev/${lvm_pv_dev} ${lvm_pv_part}
  udevadm settle --timeout=30
  partprobe -s /dev/${lvm_pv_dev}
  lvm pvresize -v ${lvm_pv_path}
  lvm vgchange --sysinit -ay
  lvm lvextend -v -l +100%FREE ${lvm_lv_root}
  e2fsck -p -f ${lvm_lv_root}
  resize2fs -p ${lvm_lv_root}
}

growroot
' >/usr/lib/dracut/modules.d/40growroot/growroot.sh

chmod +x /usr/lib/dracut/modules.d/40growroot/module-setup.sh
chmod +x /usr/lib/dracut/modules.d/40growroot/growroot.sh

# Required to allow LVM resize on boot
sed -i "s/locking_type = 4/locking_type = 1/" /usr/lib/dracut/modules.d/90lvm/module-setup.sh

kernel=`ls /lib/modules/`
/sbin/dracut --force -v --kmoddir /lib/modules/$kernel /boot/initramfs-$kernel.img $kernel

# Set domain name in hosts file
#sed -i 's/127.0.1.1\t\([a-z]*\).*/127.0.1.1\t\1\.dp\-net\.com\t\1/' /etc/hosts

# Avoid using DHCP-server provided domain name.
#sed -i 's/#supersede.*/supersede domain-name "dp-net.com";/' /etc/dhcp/dhclient.conf

#disable first boot reconfiguration
rm -rf /etc/rc.d/init.d/firstboot /.unconfigured

echo '
users:
   - default

disable_root: false
ssh_pwauth: true

datasource_list: [ Ec2, OpenStack ]

preserve_hostname: false

cloud_init_modules:
 - migrator
 - bootcmd
 - write-files
 - set_hostname
 - update_hostname
 - update_etc_hosts
 - ca-certs
 - rsyslog
 - users-groups
 - ssh

cloud_config_modules:
 - emit_upstart
 - mounts
 - locale
 - set-passwords
 - grub-dpkg
 - apt-pipelining
 - apt-configure
 - package-update-upgrade-install
 - timezone
 - disable-ec2-metadata
 - runcmd

cloud_final_modules:
 - rightscale_userdata
 - scripts-per-once
 - scripts-per-boot
 - scripts-per-instance
 - scripts-user
 - ssh-authkey-fingerprints
 - keys-to-console
 - phone-home
 - final-message
 - power-state-change

syslog_fix_perms: root:root
warnings:
  dsid_missing_source: off

system_info:
   distro: rhel
   default_user:
     name: root
     lock_passwd: false
   paths:
      cloud_dir: /var/lib/cloud/
      templates_dir: /etc/cloud/templates/
      upstart_dir: /etc/init/' > /etc/cloud/cloud.cfg
